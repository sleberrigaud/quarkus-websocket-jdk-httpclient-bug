package org.acme.getting.started;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
public class GreetingResourceWithJdkHttpClientTest {

    @Test
    public void testHelloEndpoint() {
        final var request = HttpRequest.newBuilder(URI.create("http://localhost:" + 8081))
                .GET()
                .build();

        final var status = HttpClient.newHttpClient()
                .sendAsync(request, BodyHandlers.ofString())
                .thenApply(HttpResponse::statusCode)
                .join();

        assertThat(status, is(200));
    }
}